const express = require('express');

const app = express();

const mysqlFunctions = require('.src/utils/MysqlFunction'); // 假设数据库函数文件名为 MysqlFunction.js



// 中间件，用于解析请求体中的 JSON 数据

app.use(express.json());



// 用户登录路由

app.post('/login', (req, res) => {

  const { username, password } = req.body;



  console.log(username);



  // 调用用户登录查询函数

  mysqlFunctions.UserQuery(username, password, (user) => {

    if (user) {

      // 登录成功

      res.json({ success: true, user });



      //跳转到用户接口界面

      res.sendFile(__dirname + '/interface.html');

    } else {

      // 登录失败

      console.log('false');



      res.json({ success: false, message: 'Invalid username or password.' });

    }

  });

});



//用户注册函数

app.post('/regis',(req,res) => {

  const user = req.body;



  mysqlFunctions.UserEnroll(user,(flag) =>{

    if(flag){

      //登录成功

      res.json({ success: true, user });



      //页面跳转

      res.sendFile(__dirname + '/login.html');



    } else {

      // 登录失败

      res.json({ success: false, message: 'Invalid username or password.' });

    }

    

  })



})



// 显示用户的接口路由

app.get('/user/interfaces', (req, res) => {

  // 在这里根据用户信息查询相关接口数据

  // 可以调用数据库函数，如 mysqlFunctions.InterfaceSeek

  // 并将结果返回给客户端

  res.json({ message: 'User interfaces' });

});



// 创建接口路由

app.post('/interface', (req, res) => {

  const interfaceData = req.body;



  // 调用创建接口函数

  mysqlFunctions.InterfaceFound(interfaceData, (success) => {

    if (success) {

      res.json({ success: true, message: 'Interface created successfully.' });

    } else {

      res.json({ success: false, message: 'Failed to create interface.' });

    }

  });

});



// 删除接口路由

app.delete('/interface/:id', (req, res) => {

  const interfaceId = req.params.id;



  // 调用删除接口函数

  mysqlFunctions.InterfaceDelete(interfaceId, (success) => {

    if (success) {

      res.json({ success: true, message: 'Interface deleted successfully.' });

    } else {

      res.json({ success: false, message: 'Failed to delete interface.' });

    }

  });

});



// 修改接口路由

app.put('/interface/:id', (req, res) => {

  const interfaceId = req.params.id;

  const interfaceData = req.body;



  // 调用修改接口函数

  mysqlFunctions.InterfaceChange(interfaceId, interfaceData, (success) => {

    if (success) {

      res.json({ success: true, message: 'Interface updated successfully.' });

    } else {

      res.json({ success: false, message: 'Failed to update interface.' });

    }

  });

});



// 查询接口路由

app.get('/interface/:name', (req, res) => {

  const interfaceName = req.params.name;



  // 调用查询接口函数

  mysqlFunctions.InterfaceSeek(interfaceName, (results) => {

    if (results) {

      res.json({ success: true, data: results });

    } else {

      res.json({ success: false, message: 'Failed to find interface.' });

    }

  });

});



// 创建项目接口

app.post('/project', (req, res) => {

  const project = req.body;

  const userId = req.body.userId; // 假设从请求中获取用户ID

  const userLevel = req.body.userLevel; // 假设从请求中获取用户级别



  mysqlFunctions.ProjectFound(project, userId, userLevel, (flag) => {

    if (flag) {

      // 创建项目成功w

      res.json({ success: true });

    } else {

      // 创建项目失败

      res.json({ success: false, message: 'Failed to create project.' });

    }

  });

});





// 修改项目接口

app.put('/project/:projectId', (req, res) => {

const projectId = req.params.projectId;

const updatedProject = req.body;



mysqlFunctions.ProjectChange(projectId, updatedProject, (flag) => {

  if (flag) {

    // 修改项目成功

    res.json({ success: true });

  } else {

    // 修改项目失败

    res.json({ success: false, message: 'Failed to update project.' });

  }

});

});



// 删除项目接口

app.delete('/project/:projectId', (req, res) => {

  const projectId = req.params.projectId;



  // 执行删除项目操作

  mysqlFunctions.ProjectDelete(projectId, userId, (flag) => {

    if (flag) {

      // 删除项目成功

      res.json({ success: true });

    } else {

      // 删除项目失败

      res.json({ success: false, message: 'Failed to delete project.' });

    }

  });

});



// 查询项目接口

app.get('/project/:projectId', (req, res) => {

  const projectId = req.params.projectId;



  mysqlFunctions.ProjectGet(projectId, (project) => {

    if (project) {

      // 查询项目成功

      res.json({ success: true, project });

    } else {

      // 查询项目失败

      res.json({ success: false, message: 'Failed to retrieve project.' });

    }

  });

});



// 其他路由和功能可以根据需要继续添加



// 启动服务器

const port = 3000;

app.listen(port, () => {

  console.log(`Server is running on port ${port}`);

});