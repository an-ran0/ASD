/*记录接口的修改的数据库*/

DROP TABLE IF EXISTS `changelogs`;

CREATE TABLE `changelogs` (
  `ChangeLogID` int(11) NOT NULL AUTO_INCREMENT,
  `InterfaceID` int(11) DEFAULT NULL,
  `HistoryInterface` text NOT NULL,
  `ChangeDescription` varchar(255) DEFAULT NULL,
  `ChangeTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ChangeLogID`),
  KEY `InterfaceID` (`InterfaceID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

