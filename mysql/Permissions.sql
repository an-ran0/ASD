//记录用户在各个项目中的权限设置的数据库

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `PermissionID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `ProjectID` int(11) DEFAULT NULL,
  `PermissionLevel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PermissionID`),
  KEY `UserID` (`UserID`),
  KEY `ProjectID` (`ProjectID`),
  CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`),
  CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`ProjectID`) REFERENCES `projects` (`ProjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;