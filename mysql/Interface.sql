/*接口数据库*/

DROP TABLE IF EXISTS `interfaces`;

CREATE TABLE `interfaces` (
  `InterfaceID` int(11) NOT NULL,
  `ProjectID` int(11) DEFAULT NULL,
  `InterfaceName` varchar(100) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `RequestMethod` varchar(50) DEFAULT NULL,
  `RequestParameters` varchar(255) DEFAULT NULL,
  `ResponseData` varchar(255) DEFAULT NULL,
  `CreationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`InterfaceID`),
  KEY `ProjectID` (`ProjectID`),
  CONSTRAINT `interfaces_ibfk_1` FOREIGN KEY (`ProjectID`) REFERENCES `projects` (`ProjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;