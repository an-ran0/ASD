//数据模型

const mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
  //接口名称

  apiName: { type: String, require: true },

  //访问地址

  apiUrl: { type: String, require: true },

  //请求方法

  apiMethod: { type: String, require: true },

  //请求参数

  apiParameter: { type: String, require: true },

  //创建时间

  apiTime: { type: String, require: true },
});

//将schema转化为数据模型

let user = mongoose.model('Admin', UserInterface);

module.exports = user;
