
const  UrlMap = require('./src/config/ControllerConfig')


//导入 http 模块
const http = require('http');

//创建 web 服务器实例
const server = http.createServer();


//为服务器实例绑定 request 事件，监听客户端的请求
server.on('request', function (req, res) {
  const url = req.url;
  const method = req.method;


  // 输出获取到的信息
  console.log(`method: ${method} url:  ${url},`);

  let urlfun = UrlMap.get(url);
  if (urlfun) {
    let ans = urlfun() ;
    console.log(ans);
    res.write(ans);
  }else{
    console.log(`URL ERROR`);
    res.write(`URL ERROR`);
  }


  // res.write(`${ControllerConfig.str}`);
  res.end();
});

//④ 启动服务器
server.listen('8080', () => {
  console.log('server start in 8080');
});
