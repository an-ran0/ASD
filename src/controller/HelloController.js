const HelloController = {
    hello() {
        return "hello"
    },
    helloNode() {
        return "hello NodeJs"
    },
}
module.exports = HelloController;