
const  HelloController = require('../controller/HelloController')


const UrlMap = new Map([

    ["/", () => "NULL"],
    ["/hello", HelloController.hello],
    ["/hello/node", HelloController.helloNode],

])

module.exports = UrlMap;